package com.alienlabz.encomendaz.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.simpleframework.xml.Default;
import org.simpleframework.xml.DefaultType;

/**
 * Representa uma empres que fornece serviços de postagem de encomendas/cartas.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@Default(value = DefaultType.FIELD, required = false)
@Entity
public class PostalService {

	@Id
	private Long id;

	@Column
	private String logo;

	@Column
	private String name;

	@Column
	private String url;

	@Column
	private String country;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCountry() {
		return country;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getLogo() {
		return logo;
	}

	@Override
	public String toString() {
		return name;
	}

}

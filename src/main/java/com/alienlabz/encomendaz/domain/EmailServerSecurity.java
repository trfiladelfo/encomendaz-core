package com.alienlabz.encomendaz.domain;

/**
 * Opções de segurança dos servidores de e-mail.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public enum EmailServerSecurity {
	SSL_TLS("SSL_TLS"), STARTTLS("STARTTLS"), None("");

	private String type;

	private EmailServerSecurity(String type) {
		this.setType(type);
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

}

package com.alienlabz.encomendaz.business;

import java.util.List;

import br.gov.frameworkdemoiselle.stereotype.BusinessController;
import br.gov.frameworkdemoiselle.template.DelegateCrud;
import br.gov.frameworkdemoiselle.transaction.Transactional;

import com.alienlabz.encomendaz.domain.PostalService;
import com.alienlabz.encomendaz.domain.Service;
import com.alienlabz.encomendaz.persistence.ServicePersistence;

/**
 * Trata as regras de negócio relacionadas à entidade {@link Service}.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@BusinessController
public class ServiceBC extends DelegateCrud<Service, Long, ServicePersistence> {

	/**
	 * Criar os serviços que são padrão nos Correios.
	 * 
	 * @param postalService Serviço Postal.
	 */
	@Transactional
	public void createDefaultCorreiosServices(PostalService postalService) {
		insert(new Service(1L, "PAC", postalService, "41106"));
		insert(new Service(2L, "e-Sedex", postalService, "81019"));
		insert(new Service(3L, "Sedex", postalService, "40010"));
		insert(new Service(4L, "Sedex Hoje", postalService, "40290"));
		insert(new Service(5L, "Sedex a Cobrar", postalService, "40045"));
		insert(new Service(6L, "Sedex 10", postalService, "40215"));
		insert(new Service(7L, "Carta Registrada", postalService, "10014"));
		insert(new Service(8L, "Outros", postalService, null));
	}

	/**
	 * Criar os serviços que são padrão para o serviço postal genérico.
	 * 
	 * @param postalServiceOthers Serviço Postal.
	 */
	public void createDefaultOthersServices(PostalService postalServiceOthers) {
		insert(new Service(9L, "Outros", postalServiceOthers, null));
	}

	/**
	 * Obter a lista de serviços que um Serviço Postal possui.
	 * 
	 * @param idPostalService Identificador do Serviço Postal.
	 * @return Lista de serviços.
	 */
	public List<Service> findByPostalService(Long idPostalService) {
		return getDelegate().findByPostalService(idPostalService);
	}
}

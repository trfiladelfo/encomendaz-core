package com.alienlabz.encomendaz.business;

import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.persistence.PersistenceException;

import org.alfredlibrary.AlfredException;
import org.alfredlibrary.utilitarios.correios.Rastreamento;
import org.alfredlibrary.utilitarios.correios.RegistroRastreamento;
import org.alfredlibrary.utilitarios.data.CalculoData;
import org.alfredlibrary.utilitarios.net.WorldWideWeb;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;

import br.gov.frameworkdemoiselle.stereotype.BusinessController;
import br.gov.frameworkdemoiselle.template.DelegateCrud;
import br.gov.frameworkdemoiselle.transaction.Transactional;
import br.gov.frameworkdemoiselle.util.Beans;
import br.gov.frameworkdemoiselle.util.Strings;

import com.alienlabz.encomendaz.domain.State;
import com.alienlabz.encomendaz.domain.Tracking;
import com.alienlabz.encomendaz.domain.TrackingStatus;
import com.alienlabz.encomendaz.exception.CodeExistsException;
import com.alienlabz.encomendaz.exception.EmailPreferencesNotDefinedException;
import com.alienlabz.encomendaz.exception.InsufficientParameters;
import com.alienlabz.encomendaz.exception.PersonEmailNotDefinedException;
import com.alienlabz.encomendaz.exception.ValorDeclaradoMenorMinimoException;
import com.alienlabz.encomendaz.persistence.TrackingPersistence;
import com.alienlabz.encomendaz.preferences.UserPreferences;
import com.alienlabz.encomendaz.util.DateUtil;
import com.alienlabz.encomendaz.util.GeoLocationUtil;
import com.alienlabz.encomendaz.util.MailSender;
import com.alienlabz.encomendaz.util.NumberUtil;
import com.alienlabz.encomendaz.util.SearchBundle;

/**
 * Responsável pelas regras de negócio relacionadas às postagens.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@BusinessController
public class TrackingBC extends DelegateCrud<Tracking, Long, TrackingPersistence> {
	private static final long serialVersionUID = 1L;
	public static final double MINIMO_VALOR_DECLARADO = 11.0;

	@Inject
	private MailSender mail;

	@Inject
	private Logger logger;

	@Inject
	private MailSender mailSender;

	@Inject
	private UserPreferences userPreferences;

	/**
	 * 
	 * @param tracking
	 */
	public void sendEmail(final Tracking tracking) {
		if (userPreferences.getEmailServer() == null || "".equals(userPreferences.getEmailServer())) {
			throw new EmailPreferencesNotDefinedException();
		}
		if (userPreferences.getEmailServerPort() == 0) {
			throw new EmailPreferencesNotDefinedException();
		}
		if (userPreferences.getEmailUsername() == null || "".equals(userPreferences.getEmailUsername())) {
			throw new EmailPreferencesNotDefinedException();
		}
		if (userPreferences.getEmailPassword() == null || "".equals(userPreferences.getEmailPassword())) {
			throw new EmailPreferencesNotDefinedException();
		}
		if (userPreferences.getEmailSecurity() == null) {
			throw new EmailPreferencesNotDefinedException();
		}
		if (tracking.getTo() == null || tracking.getTo().getEmail() == null || "".equals(tracking.getTo().getEmail())) {
			throw new PersonEmailNotDefinedException();
		}
		mail.notify(tracking, tracking.getTo());
	}

	/**
	 * Obter o preço para o envio da encomenda.
	 * 
	 * FIXME Por enquanto, calculando apenas para os Correios!
	 * 
	 * @param tracking Encomenda.
	 * @return Preço.
	 */
	public Double getPrice(final Tracking tracking) {
		logger.debug("obtendo o preço para o envio de uma encomenda.");

		if (tracking == null || tracking.getService() == null || tracking.getPack() == null
				|| tracking.getFrom() == null || tracking.getTo() == null) {
			throw new InsufficientParameters();
		}

		Map<String, String> parametros = new HashMap<String, String>();
		parametros.put("resposta", "paginaCorreios");
		parametros.put("servico", tracking.getService().getExternalCode());
		parametros.put("Comprimento", String.valueOf(tracking.getPack().getBreadth()));
		parametros.put("Altura", String.valueOf(tracking.getPack().getAltitude()));
		parametros.put("Diametro", String.valueOf(tracking.getPack().getGirth()));
		parametros.put("Largura", String.valueOf(tracking.getPack().getLength()));
		parametros.put("cepOrigem", tracking.getFrom().getPostalCode());
		parametros.put("cepDestino", tracking.getTo().getPostalCode());
		parametros.put("embalagem", "");
		if (tracking.getDeclaredValue() != null && tracking.getDeclaredValue() > 0) {
			if (tracking.getDeclaredValue() < MINIMO_VALOR_DECLARADO) {
				throw new ValorDeclaradoMenorMinimoException();
			}
			parametros.put("valorD", NumberUtil.convertCurrency(tracking.getDeclaredValue()));
			parametros.put("valorDeclarado", NumberUtil.convertCurrency(tracking.getDeclaredValue()));
		} else {
			parametros.put("valorD", "");
			parametros.put("valorDeclarado", "");
		}
		parametros.put("peso", tracking.getWeight().intValue() + "");

		Map<String, String> cabecalhos = new HashMap<String, String>();
		cabecalhos.put("referer", "http://www.correios.com.br/encomendas/prazo/default.cfm");
		try {
			String conteudo = WorldWideWeb.obterConteudoSite("http://www.correios.com.br/encomendas/prazo/prazo.cfm",
					parametros, cabecalhos);

			Pattern padrao = Pattern.compile("<b>R\\$ \\d{1,3},\\d{2}</b>");
			Matcher pesquisa = padrao.matcher(conteudo);

			String preco = "";
			while (pesquisa.find()) {
				preco = pesquisa.group();
			}

			if ("".equals(preco)) {
				return 0D;
			}

			return Double.valueOf(preco.replace("<b>", "").replace("</b>", "").replace("R$", "").replace(",", "."));
		} catch (Throwable e) {
			return 0D;
		}
	}

	/**
	 * Verificar se a postagem passada teve uma movimentação recente.
	 * 
	 * @param tracking Rastreamento a ser verificado.
	 * @return Verdadeiro caso tenha uma movimentação no dia, falso caso contrário.
	 */
	public boolean hasChangesToday(final Tracking tracking) {
		logger.debug("verificando se houve mudança de status para a encomenda " + tracking.getCode());

		boolean result = false;
		for (TrackingStatus status : tracking.getStatuses()) {
			if (DateUtil.isToday(status.getDate())) {
				result = true;
				break;
			}
		}
		return result;
	}

	/**
	 * Obter uma lista de todos os estados possíveis de um rastreamento.
	 * 
	 * @return Lista de estados.
	 */
	public List<State> getAllStates() {
		logger.debug("obtendo a lista de estados.");
		List<State> result = new ArrayList<State>();
		for (State state : State.values()) {
			result.add(state);
		}
		return result;
	}

	/**
	 * Obter a lista de encomendas que estão em atraso.
	 * 
	 * @param searchBundle
	 * @return
	 */
	public List<Tracking> findOverdueTrackings(final SearchBundle searchBundle) {
		logger.debug("obtendo os rastreamentos que estão em atraso.");

		List<Tracking> result = new ArrayList<Tracking>();
		List<Tracking> notDeliveredTrackings = findInTransitTrackings(searchBundle);
		for (Tracking tracking : notDeliveredTrackings) {
			int days = CalculoData.calcularDiferencaDias(tracking.getSent(), new Date());
			if (tracking.getEstimative() != null && tracking.getEstimative() > 0 && days > tracking.getEstimative()) {
				result.add(tracking);
			}
		}
		return result;
	}

	/**
	 * Encontrar todas as encomendas que possuem a pessoa informada como remetente ou destinatário.
	 * 
	 * @param idPerson Identificador da pessoa.
	 * @return Lista de encomendas encontradas.
	 */
	public List<Tracking> findTrackingsByPerson(final Long idPerson) {
		logger.debug("obtendo os rastreamentos que uma determinada pessoa é remetente ou destinatário.");
		return getDelegate().findTrackingsByPerson(idPerson);
	}

	/**
	 * Remover todos os registros de rastreamentos da base de dados.
	 */
	@Transactional
	public void deleteAll() {
		logger.debug("removendo todos os registros de rastreamento.");

		List<Tracking> trackings = findAll();
		for (Tracking tracking : trackings) {
			delete(tracking.getId());
		}
		findAll(); // FIXME Parece que a Sessão do Hibernate não se atualiza enquanto não disparamos um findAll().
	}

	/**
	 * Insere um novo registro de rastreamento sem checar nos Correios por novos status.
	 * 
	 * @param tracking Rastreamento a ser inserido.
	 */
	public void rawInsert(final Tracking tracking) {
		logger.debug("inserindo um rastreamento sem dados da internet.");
		super.insert(tracking);
	}

	/**
	 * Obter da Internet os status do rastreamento e inserir no objeto.
	 * 
	 * @param tracking Rastreamento.
	 */
	private void fillWithStatuses(final Tracking tracking) {
		List<RegistroRastreamento> registros = Rastreamento.rastrear(tracking.getCode());
		List<TrackingStatus> statuses = new LinkedList<TrackingStatus>();
		for (RegistroRastreamento registro : registros) {
			statuses.add(convert(registro));
		}
		tracking.setStatuses(statuses);
	}

	/**
	 * Tratar a inserção para obter a lista de situações dos Correios.
	 */
	@Override
	@Transactional
	public void insert(final Tracking tracking) {
		try {
			logger.debug("inserindo um rastreamento.");

			tracking.setDate(new Date());
			tracking.setArchived(false);
			fillWithStatuses(tracking);
			setTaxed(tracking);
			if (tracking.getSent() == null && tracking.getFirstStatus() != null) {
				tracking.setSent(tracking.getFirstStatus().getDate());
			}
		} catch (AlfredException exception) {
		}
		try {
			PersonBC personBC = Beans.getReference(PersonBC.class);
			if (tracking.getFrom() != null && tracking.getFrom().getId() == null) {
				personBC.insert(tracking.getFrom());
			}
			if (tracking.getTo() != null && tracking.getTo().getId() == null) {
				personBC.insert(tracking.getTo());
			}
			setState(tracking);
			super.insert(tracking);
		} catch (PersistenceException exception) {
			handlePersistenceException(exception);
		}
	}

	/**
	 * Desfazer o relacionamento que uma determinada pessoa possui com os rastreamentos existentes.
	 * 
	 * @param idPerson ID da pessoa.
	 */
	public void updateTrackingsPeopleToNull(final long idPerson) {
		logger.debug("removendo o relacionamento da pessoa " + idPerson
				+ " com todas as encomendas em que ela é destinatário ou remetente.");

		List<Tracking> trackings = findTrackingsByPerson(idPerson);
		for (Tracking tracking : trackings) {
			if (tracking.getTo() != null && tracking.getTo().getId().equals(idPerson)) {
				tracking.setTo(null);
			}
			if (tracking.getFrom() != null && tracking.getFrom().getId().equals(idPerson)) {
				tracking.setFrom(null);
			}
			update(tracking);
		}
	}

	@Override
	public void update(final Tracking bean) {
		try {
			super.update(bean);
		} catch (PersistenceException exception) {
			handlePersistenceException(exception);
		}
	}

	/**
	 * Definir a situação do rastreamento.
	 * 
	 * @param tracking Rastreamento.
	 */
	private void setState(final Tracking tracking) {
		if (tracking.getStatuses() == null || tracking.getStatuses().size() == 0) {
			tracking.setState(State.Unregistered);
			return;
		}
		String status = (tracking.getLastStatus() != null ? tracking.getLastStatus().getStatus() + " "
				+ tracking.getLastStatus().getDetails() : "").toLowerCase();
		if (status.contains("postado")) {
			tracking.setState(State.Posted);
		} else if (status.contains("entregue") || status.contains("entrega efetuada")) {
			tracking.setState(State.Delivered);
		} else if (status.contains("mal encaminhado")) {
			tracking.setState(State.BadRouted);
		} else if (status.contains("conferido")) {
			tracking.setState(State.Checked);
		} else if (status.contains("aguardando retirada")) {
			tracking.setState(State.WaitingReceiver);
		} else if (status.contains("exercito")) {
			tracking.setState(State.InSupervisionArmy);
		} else if (status.contains("fiscalização")) {
			tracking.setState(State.InSupervision);
		} else if (status.contains("devolvido")) {
			tracking.setState(State.Returned);
		} else if (status.contains("devolução internacional")) {
			tracking.setState(State.InternationalReturn);
		} else if (status.contains("saiu para entrega")) {
			tracking.setState(State.OutForDelivery);
		} else if (status.contains("não procurado")) {
			tracking.setState(State.Unsought);
		} else if (status.contains("encaminhado")) {
			tracking.setState(State.Routed);
		}
	}

	/**
	 * Verificar se a encomenda passada como parâmetro vem de um remetente no exterior.
	 * FIXME Para definir se a encomenda é internacional ou não, depende de quem está executando o programa. :)
	 * 
	 * @param tracking Rastreamento.
	 * @return Verdadeiro, caso venha de um remetente do exterior.
	 */
	public boolean isInternational(final Tracking tracking) {
		logger.debug("verificando se uma encomenda é internacional.");
		return tracking != null && tracking.getCode() != null && !tracking.getCode().endsWith("BR");
	}

	/**
	 * Verificar se a situação indica que a encomenda foi taxada.
	 * FIXME A verificação se está tributada ou não depende do serviço de correios de cada país.
	 * 
	 * @param status Situação.
	 * @return Verdadeiro se foi taxada.
	 */
	public void setTaxed(final Tracking tracking) {
		logger.debug("definindo se uma encomenda foi taxada ou não.");
		for (TrackingStatus status : tracking.getStatuses()) {
			if (status.getDetails() != null) {
				if (status.getDetails().toLowerCase().indexOf("tributado") > -1
						|| status.getDetails().toLowerCase().indexOf("tributada") > -1) {
					tracking.setTaxed(true);
				}
			}
		}
	}

	/**
	 * Tratar quando ocorre uma exceção relacionada à camada de persistência.
	 * 
	 * @param exception Exceção lançada pela camada de persistência.
	 */
	private void handlePersistenceException(final Throwable exception) {
		Throwable cause = exception;
		if (exception != null && exception.getMessage() != null && exception.getCause() != null
				&& exception.getCause().getCause() != null && exception.getCause().getCause().getMessage() != null
				&& exception.getCause().getCause().getMessage().toLowerCase().indexOf("unique") > -1) {
			throw new CodeExistsException();
		} else {
			if (cause != null) {
				handlePersistenceException(cause.getCause());
			}
		}
	}

	/**
	 * Obter a lista de todas as postagens marcadas como importantes pelo usuário.
	 * 
	 * @return Lista de postagens importantes.
	 */
	public List<Tracking> findImportantTrackings(final SearchBundle bundle) {
		logger.debug("obtendo apenas os rastreamentos importantes.");
		return getDelegate().findImportantTrackings(bundle);
	}

	/**
	 * Obter a lista de todas as postagens que não foram arquivadas pelo usuário.
	 * 
	 * @return Lista de postagens não arquivadas.
	 */
	public List<Tracking> findNotArchivedTrackings(final SearchBundle bundle) {
		logger.debug("obtendo todos os rastreamentos que não estão arquivados.");
		return getDelegate().findNotArchivedTrackings(bundle);
	}

	/**
	 * Obter a lista de todas as postagens que não está ainda disponíveis.
	 * 
	 * @return Lista de postagens ainda não disponíveis.
	 */
	public List<Tracking> findUnregisteredTrackings(final SearchBundle bundle) {
		logger.debug("obtendo apenas os rastreamentos que ainda não estão disponíveis.");
		return getDelegate().findByState(bundle, State.Unregistered);
	}

	/**
	 * Obter a lista de todas as postagens que tiveram mudança de situação recente.
	 * 
	 * @return Lista de postagens com mudança situação recente.
	 */
	public List<Tracking> findRecentTrackings(final SearchBundle bundle) {
		logger.debug("obtendo apenas os rastreamentos que tiveram mudanças recentes.");
		List<Tracking> trackings = findNotArchivedTrackings(bundle);
		List<Tracking> result = new ArrayList<Tracking>();
		for (Tracking tracking : trackings) {
			if (hasChangesToday(tracking)) {
				result.add(tracking);
			}
		}
		return result;
	}

	/**
	 * Obter a lista de todas as postagens que ainda estão em trânsito.
	 * 
	 * @return Lista de postagens que ainda estão em trânsito.
	 */
	public List<Tracking> findInTransitTrackings(final SearchBundle bundle) {
		logger.debug("obtendo todos os rastreamentos que estão ainda em trânsito.");
		return getDelegate().findByNotInState(bundle, State.Unregistered, State.Delivered, State.Returned,
				State.InternationalReturn);
	}

	/**
	 * Obter a lista de todas as postagens que já foram entregues.
	 * 
	 * @return Lista de postagens entregues.
	 */
	public List<Tracking> findDeliveredTrackings(final SearchBundle bundle) {
		logger.debug("obtendo todos os rastreamentos já entregues.");
		return getDelegate().findByState(bundle, State.Delivered);
	}

	/**
	 * Obter a lista de todas as postagens que ainda não foram entregues.
	 * É diferente da lista de postagens em trânsito, pois aqui também incluem as postagens não registradas.
	 * 
	 * @return Lista.
	 */
	public List<Tracking> findNotDeliveredTrackings(final SearchBundle bundle) {
		logger.debug("obtendo todos os rastreamentos que ainda não foram entregues.");
		List<Tracking> trackings = new ArrayList<Tracking>();
		trackings.addAll(findInTransitTrackings(bundle));
		trackings.addAll(findUnregisteredTrackings(bundle));
		return trackings;
	}

	/**
	 * Obter a lista de todas as postagens que estão arquivadas pelo usuário.
	 * 
	 * @return Lista de postagens arquivadas.
	 */
	public List<Tracking> findArchivedTrackings() {
		logger.debug("obtendo todos os rastreamentos que estão arquivados.");
		return null;
	}

	/**
	 * Obter um rastreamento diretamente do serviço postal na internet, ignorando o banco de dados.
	 * FIXME Por enquanto, os dados vem apenas dos Correios do Brasil.
	 * 
	 * @param code Código do rastreamento.
	 * @return
	 */
	public Tracking getTrackingFromNetwork(final String code) {
		logger.debug("obtendo os dados de um rastreamento da internet.");
		Tracking tracking = new Tracking();
		tracking.setCode(code);
		fillWithStatuses(tracking);
		setState(tracking);
		setTaxed(tracking);
		tracking.setSent((tracking.getFirstStatus() != null ? tracking.getFirstStatus().getDate() : null));
		return tracking;
	}

	/**
	 * Obter uma lista contendo as coordenadas por onde a encomenda passou.
	 * 
	 * @param tracking Encomenda.
	 * @return Lista contendo Arrays de Doubles que demonstram por onde o pacote passou.
	 */
	public List<double[]> getGeoCoordinates(final Tracking tracking) {
		logger.debug("obtendo as coordenadas geográficas nas quais uma encomenda passou.");

		List<double[]> coords = new LinkedList<double[]>();
		for (int i = (tracking.getStatuses().size() - 1); i >= 0; i--) {
			TrackingStatus status = tracking.getStatuses().get(i);
			if (status.getPlace() != null && !Strings.isEmpty(status.getPlace())) {
				String[] c = GeoLocationUtil.getGeoLocationFromYahoo(status.getPlace());
				if ("-14.242916".equals(c[0]) && "-51.412289".equals(c[1])) {
					continue;
				}
				coords.add(new double[] { NumberUtils.toDouble(c[0]), NumberUtils.toDouble(c[1]) });
			}
		}
		return coords;
	}

	/**
	 * Verificar se há mudanças de situação nas encomendas que ainda não foram entregues.
	 * 
	 * @return Lista de encomendas que sofreram alterações.
	 */
	@Transactional
	public List<Tracking> verifyTrackings() {
		logger.debug("verificando se há mudanças nas encomendas que ainda não foram entregues.");

		List<Tracking> toCheck = this.findNotDeliveredTrackings(new SearchBundle(""));
		List<Tracking> modified = new ArrayList<Tracking>();
		for (Tracking tracking : toCheck) {
			TrackingStatus status = tracking.getLastStatus();
			try {
				List<RegistroRastreamento> registros = Rastreamento.rastrear(tracking.getCode());
				if (!registros.isEmpty()) {
					TrackingStatus newStatus = convert(registros.iterator().next());
					if (!newStatus.equals(status)) {
						modified.add(tracking);
						update(tracking, registros);
						if (userPreferences.isSendAutomaticEmails()) {
							notifyPersons(tracking);
						}
					}
				}
			} catch (AlfredException alfredException) {
				logger.debug("código de rastreamento não encontrado.");
			}
		}
		return modified;
	}

	/**
	 * Realizar a notificação dos interessados na encomenda.
	 * 
	 * @param tracking Encomenda.
	 */
	private void notifyPersons(final Tracking tracking) {
		if (tracking.getTo() != null && !Strings.isEmpty(tracking.getTo().getEmail())) {
			if (tracking.getTo().getPreferences() != null && tracking.getTo().getPreferences().isNotifyByMail()) {
				mailSender.notify(tracking, tracking.getTo());
			}
		}
		if (tracking.getFrom() != null && !Strings.isEmpty(tracking.getFrom().getEmail())) {
			if (tracking.getFrom().getPreferences() != null && tracking.getFrom().getPreferences().isNotifyByMail()) {
				mailSender.notify(tracking, tracking.getFrom());
			}
		}
	}

	/**
	 * Atualizar um rastreamento, adicionando a ele os registros de rastreamentos passados como parâmetro.
	 * 
	 * @param tracking Rastreamento a ser atualizado.
	 * @param registros Registros de rastreamento que serão associados.
	 */
	@Transactional
	public void update(final Tracking tracking, final List<RegistroRastreamento> registros) {
		logger.debug("atualizando um rastreamento.");

		tracking.getStatuses().clear();
		update(tracking);
		for (RegistroRastreamento registro : registros) {
			tracking.getStatuses().add(convert(registro));
		}
		setTaxed(tracking);
		setState(tracking);
		update(tracking);
	}

	/**
	 * Converter um rastreamento do Alfred para o do sistema.
	 * 
	 * @param register Registro de rastreamento.
	 * @return Status convertido.
	 */
	private TrackingStatus convert(final RegistroRastreamento register) {
		TrackingStatus status = new TrackingStatus();
		status.setStatus(register.getAcao());
		status.setDate(register.getDataHora());
		status.setDetails(register.getDetalhe());
		status.setPlace(register.getLocal());
		return status;
	}

	/**
	 * Arquivar uma encomenda.
	 * 
	 * @param tracking Encomenda a ser arquivada.
	 */
	public void archive(final Tracking tracking) {
		tracking.setArchived(true);
		update(tracking);
	}

	/**
	 * Obter qual o endereço para ver no browser o rastreamento.
	 * FIXME Obviamente, depende do serviço de correio usado. Aqui está fixo para os Correios do Brasil. 
	 * 
	 * @param tracking Rastreamento.
	 * @return URI.
	 */
	public URI getTrackingServiceURI(final Tracking tracking) {
		String strURL = "http://websro.correios.com.br/sro_bin/txect01$.QueryList?P_LINGUA=001&P_TIPO=001&P_COD_UNI="
				+ tracking.getCode();
		return URI.create(strURL);
	}

	/**
	 * Realizar a importação de um lote de códigos separados por um caracter.
	 * 
	 * @param codes Códigos.
	 * @param separator Separador dos códigos.
	 */
	public void batchImport(final String codes, final String separator) {
		logger.debug("importando uma lista de códigos de rastreamento.");

		if (!Strings.isEmpty(codes) && !Strings.isEmpty(separator)) {
			String[] aCodes = codes.split(separator);
			for (String c : aCodes) {
				Tracking tracking = new Tracking();
				tracking.setCode(c.trim());
				try {
					insert(tracking);
				} catch (Exception exception) {

				}
			}
		}
	}

	/**
	 * Realizar uma pesquisa a partir das informações contidas no rastreamento passado como parâmetro.
	 * 
	 * @param tracking Rastreamento.
	 */
	public List<Tracking> search(final Tracking tracking) {
		logger.debug("realizando a busca de rastreamentos.");
		return getDelegate().search(tracking);
	}

	/**
	 * Obter o total de encomendas que foram entregues.
	 * 
	 * @return Total.
	 */
	public Long countDeliveredTrackings() {
		logger.debug("verificando a quantidade de encomendas entregues.");
		return getDelegate().countDeliveredTrackings();
	}

	/**
	 * Obter o total de encomendas que estão em trânsito.
	 * 
	 * @return Total.
	 */
	public Long countInTransitTrackings() {
		logger.debug("verificando a quantidade de encomendas em trânsito.");
		return getDelegate().countInTransitTrackings();
	}

	/**
	 * Obter o total de encomendas que não foram registradas.
	 * 
	 * @return Total.
	 */
	public Long countUnregisteredTrackings() {
		logger.debug("verificando a quantidade de encomendas não registradas.");
		return getDelegate().countUnregisteredTrackings();
	}

	/**
	 * Obter o total de encomendas arquivadas.
	 * 
	 * @return Total.
	 */
	public Long countNotArchivedTrackings() {
		logger.debug("verificando a quantidade de encomendas arquivadas.");
		return getDelegate().countNotArchivedTrackings();
	}

	public boolean existsTrackingsByPerson(final Long id) {
		return (getDelegate().countTrackingsByPerson(id) > 0);
	}

	/**
	 * Obter a lista de códigos preparados para exportação.
	 *  
	 * @param separator Separador dos códigos.
	 * @return Códigos.
	 */
	public String getCodesToExport(final String separator) {
		StringBuilder result = new StringBuilder();
		List<Tracking> trackings = findAll();
		for (Tracking tracking : trackings) {
			result.append(tracking.getCode());
			result.append(separator);
		}
		if (result.length() > 0) {
			result.deleteCharAt(result.length() - 1);
		}
		return result.toString();
	}

}
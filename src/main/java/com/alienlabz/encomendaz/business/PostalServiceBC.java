package com.alienlabz.encomendaz.business;

import javax.inject.Inject;

import com.alienlabz.encomendaz.domain.PostalService;
import com.alienlabz.encomendaz.persistence.jpa.PostalServicePersistenceImpl;

import br.gov.frameworkdemoiselle.annotation.Startup;
import br.gov.frameworkdemoiselle.stereotype.BusinessController;
import br.gov.frameworkdemoiselle.template.DelegateCrud;
import br.gov.frameworkdemoiselle.transaction.Transactional;

/**
 * Trata as regras de negócio relacionadas à entidade de {@link PostalService}.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@BusinessController
public class PostalServiceBC extends DelegateCrud<PostalService, Long, PostalServicePersistenceImpl> {
	private static final long DEFAULT_POSTAL_SERVICE_ID = 1L;
	
	@Inject
	private ServiceBC serviceBC;

	/**
	 * Verificar se os serviços postais já estão cadastrados.
	 */
	@Startup
	@Transactional
	public void startup() {
		if (findAll().size() <= 0) {
			PostalService postalService = new PostalService();
			postalService.setId(1L);
			postalService.setCountry("Brasil");
			postalService.setLogo("correios.png");
			postalService.setName("Correios");
			postalService.setUrl("http://www.correios.com.br/");
			insert(postalService);
			serviceBC.createDefaultCorreiosServices(postalService);

			PostalService postalServiceOthers = new PostalService();
			postalServiceOthers.setId(2L);
			postalServiceOthers.setCountry("Brasil");
			postalServiceOthers.setLogo("others_postal_services.png");
			postalServiceOthers.setName("Outros");
			insert(postalServiceOthers);
			serviceBC.createDefaultOthersServices(postalServiceOthers);
		}
	}

	/**
	 * Obter o serviço postal padrão a ser usado.
	 * 
	 * @return Serviço Postal padrão.
	 */
	public PostalService getDefaultPostalService() {
		return load(DEFAULT_POSTAL_SERVICE_ID);
	}

}

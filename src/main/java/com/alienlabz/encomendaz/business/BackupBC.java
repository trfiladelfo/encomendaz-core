package com.alienlabz.encomendaz.business;

import java.io.File;
import java.io.FileFilter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.strategy.TreeStrategy;
import org.simpleframework.xml.strategy.Type;
import org.simpleframework.xml.stream.NodeMap;

import br.gov.frameworkdemoiselle.stereotype.BusinessController;
import br.gov.frameworkdemoiselle.transaction.Transactional;

import com.alienlabz.encomendaz.domain.Backup;
import com.alienlabz.encomendaz.domain.Tracking;
import com.alienlabz.encomendaz.exception.BackupException;
import com.alienlabz.encomendaz.util.TrackingList;

/**
 * Controlador de negócio para os backups da base de dados do sistema.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@BusinessController
public class BackupBC {

	@Inject
	private TrackingBC trackingBC;

	@Inject
	private PersonBC personBC; 

	/**
	 * Obter uma lista contendo o nome dos backups disponíveis para o usuário.
	 * 
	 * @return Lista de backups.
	 */
	public List<Backup> findAvailableBackups() {
		List<Backup> backups = new ArrayList<Backup>();

		File dirFile = new File("./EncomendaZ/Backups");
		File[] files = dirFile.listFiles(new FileFilter() {

			@Override
			public boolean accept(File pathname) {
				return pathname.isFile() && pathname.getName().contains(".xml");
			}

		});
		if (files != null) {
			for (File file : files) {
				if (file.isFile()) {
					Backup backup = new Backup();
					backup.setName(file.getName());
					backup.setDate(new Date(file.lastModified()));
					backups.add(backup);
				}
			}
		}

		return backups;
	}

	/**
	 * Criar um backup do estado atual do sistema.
	 */
	public void createBackup() {
		TrackingList trackingList = new TrackingList();
		trackingList.setTrackings(trackingBC.findAll());

		Serializer serializer = new Persister(new TreeStrategy() {

			@SuppressWarnings("rawtypes")
			@Override
			public boolean write(Type type, Object value, NodeMap node, Map map) {
				if (node.getName().equals("statuses")) {
					node.put("class", "java.util.ArrayList");
				}
				return false;
			}

		});
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");

		File dir = new File("./EncomendaZ/Backups");
		if (!dir.exists()) {
			dir.mkdir();
		}

		File result = new File("./EncomendaZ/Backups/backup-" + simpleDateFormat.format(new Date()) + ".xml");

		try {
			if (!result.exists()) {
				result.createNewFile();
			} else {
				result.delete();
				result.createNewFile();
			}
			serializer.write(trackingList, result);
		} catch (Exception e) {
			throw new BackupException(e);
		}
	}

	/**
	 * Remover um backup.
	 * 
	 * @param backup Backup a ser removido.
	 */
	public void deleteBackup(Backup backup) {
		File file = new File("./EncomendaZ/Backups/" + backup.getName());
		file.delete();
	}

	/**
	 * Recuperar um determinado backup.
	 * 
	 * @param backup Backup que será usado para a recuperação.
	 */
	@Transactional
	public void recovery(Backup backup) {
		File file = new File("./EncomendaZ/Backups/" + backup.getName());
		Serializer serializer = new Persister();
		try {
			trackingBC.deleteAll();
			TrackingList list = serializer.read(TrackingList.class, file);
			for (Tracking tracking : list.getTrackings()) {
				if (tracking.getFrom() != null) {
					personBC.insertIfNotExists(tracking.getFrom());
				}
				if (tracking.getTo() != null) {
					personBC.insertIfNotExists(tracking.getTo());
				}
				trackingBC.rawInsert(tracking);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new BackupException("Não foi possível recuperar o backup selecionado.", e);
		}
	}

}

package com.alienlabz.encomendaz.exception;

/**
 * Exceção lançada quando já um problema relacionado ao template de e-mail.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class EmailTemplateException extends RuntimeException {
	private static final long serialVersionUID = 1L;

}

package com.alienlabz.encomendaz.util;

import java.util.Calendar;
import java.util.Date;

/**
 * Classe utilitária para datas.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class DateUtil {

	/**
	 * Verificar se a data passada como parâmetro é o dia atual.
	 * 
	 * @param date
	 *            Data a ser verificada.
	 * @return Verdadeiro caso a data seja hoje.
	 */
	public static boolean isToday(Date date) {
		boolean result = false;

		Calendar calendarDate = Calendar.getInstance();
		calendarDate.setTime(date);

		Calendar calendarToday = Calendar.getInstance();
		calendarToday.setTime(new Date());

		if (calendarToday.get(Calendar.DAY_OF_MONTH) == calendarDate.get(Calendar.DAY_OF_MONTH)) {
			if (calendarToday.get(Calendar.MONTH) == calendarDate.get(Calendar.MONTH)) {
				if (calendarToday.get(Calendar.YEAR) == calendarDate.get(Calendar.YEAR)) {
					result = true;
				}
			}
		}

		return result;
	}

}

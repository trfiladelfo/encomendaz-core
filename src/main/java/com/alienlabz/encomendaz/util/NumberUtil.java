package com.alienlabz.encomendaz.util;

/**
 * Utilitário para tratar a conversão de valores numéricos em string para tipos primitivos.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
final public class NumberUtil {

	/**
	 * Converter para Double.
	 * 
	 * @param value Valor a ser convertido.
	 * @return Valor convertido.
	 */
	public static Double toDouble(String value) {
		Double retorno = 0D;
		try {
			retorno = Double.valueOf(value);
		} catch (Exception e) {

		}
		return retorno;
	}

	/**
	 * Converter um valor de moeda para double.
	 * 
	 * @param value Valor.
	 * @return Valor convertido.
	 */
	public static Double convertCurrency(String value) {
		return toDouble(value.replace(".", "").replace(",", "."));
	}

	public static String convertCurrency(Double value) {
		java.text.DecimalFormat df = new java.text.DecimalFormat("###,###,##0.00");
		return df.format(value);
	}

	/**
	 * Converter um valor em uma string para um inteiro.
	 * 
	 * @param value Valor a ser convertido.
	 * @return Valor convertido.
	 */
	public static int toInteger(String value) {
		Integer retorno = 0;
		try {
			retorno = Integer.valueOf(value);
		} catch (Exception e) {

		}
		return retorno;
	}

}

package com.alienlabz.encomendaz.persistence.jpa;

import br.gov.frameworkdemoiselle.stereotype.PersistenceController;
import br.gov.frameworkdemoiselle.template.JPACrud;
import br.gov.frameworkdemoiselle.transaction.Transactional;

import com.alienlabz.encomendaz.domain.Person;
import com.alienlabz.encomendaz.persistence.PersonPersistence;

/**
 * Trata da persistência dos dados da entidade Person através de JPA.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@PersistenceController
public class PersonPersistenceImpl extends JPACrud<Person, Long> implements PersonPersistence {
	private static final long serialVersionUID = 1L;

	// FIXME Não é a melhor ideia fazer isto aqui, mas o controle transacional abafa a exceção e ela não pode ser tratada no BC.
	@Override
	@Transactional
	public void insertIfNotExists(Person person) {
		try {
			if (load(person.getId()) == null) {
				person.setId(null);
				insert(person);
			}
		} catch (Throwable e) {
			person.setId(null);
			insert(person);
		}
	}

}

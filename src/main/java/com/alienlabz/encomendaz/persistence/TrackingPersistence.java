package com.alienlabz.encomendaz.persistence;

import java.util.List;

import com.alienlabz.encomendaz.domain.State;
import com.alienlabz.encomendaz.domain.Tracking;
import com.alienlabz.encomendaz.util.SearchBundle;

import br.gov.frameworkdemoiselle.template.Crud;

/**
 * Define o comportamento padrão para a persistência de Rastreamentos.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public interface TrackingPersistence extends Crud<Tracking, Long> {

	/**
	 * Obter uma lista de postagens que tenha os dados de busca e os estados informados.
	 * 
	 * @param bundle Dados da Busca.
	 * @param state Estados.
	 * @return Lista de postagens.
	 */
	public List<Tracking> findByState(SearchBundle bundle, State... state);

	/**
	 * Obter uma lista de postagens que tenha os dados de busca e não estejam nos estados informados.
	 * 
	 * @param bundle Dados da Busca.
	 * @param state Estados.
	 * @return Lista de postagens.
	 */
	public List<Tracking> findByNotInState(SearchBundle bundle, State... state);

	/**
	 * Obter a lista de todas as postagens marcadas como importantes pelo usuário.
	 * 
	 * @param Dados adicionais para pesquisa.
	 * @return Lista de postagens importantes.
	 */
	public List<Tracking> findImportantTrackings(SearchBundle bundle);

	/**
	 * Obter a lista de todas as postagens que não foram arquivadas pelo usuário.
	 * 
	 * @param Dados adicionais para pesquisa.
	 * @return Lista de postagens não arquivadas.
	 */
	public List<Tracking> findNotArchivedTrackings(SearchBundle bundle);

	/**
	 * Encontrar todas as encomendas que possuem a pessoa informada como remetente ou destinatário.
	 * 
	 * @param idPerson Identificador da pessoa.
	 * @return Lista de encomendas encontradas.
	 */
	public List<Tracking> findTrackingsByPerson(Long idPerson);

	/**
	 * Realizar uma pesquisa a partir das informações contidas no rastreamento passado como parâmetro.
	 * 
	 * @param tracking Rastreamento.
	 */
	public List<Tracking> search(Tracking tracking);

	/**
	 * Obter a quantidade de encomendas entregues.
	 * 
	 * @return Total de Encomendas.
	 */
	public Long countDeliveredTrackings();

	/**
	 * Obter a quantidade de encomendas em trânsito.
	 * 
	 * @return Total de Encomendas.
	 */
	public Long countInTransitTrackings();
	
	/**
	 * Obter a quantidade de encomendas não registradas.
	 * 
	 * @return Total de Encomendas.
	 */
	public Long countUnregisteredTrackings();

	/**
	 * Obter a quantidade de encomendas não arquivadas.
	 * 
	 * @return Total de Encomendas.
	 */
	public Long countNotArchivedTrackings();

	/**
	 * Verificar em quantos rastreamentos uma pessoa está associada. 
	 * 
	 * @param id ID da pessoa.
	 * @return Quantidade de rastreamentos.
	 */
	public int countTrackingsByPerson(final Long id);
}
